nImages=1000
make 
./prog $nImages
python3 convert.py ../video/ jpg
ffmpeg -framerate 35 -i ../video/result%05d.jpg -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p ../videos/video.mp4
rm ../video/*
