import os
import sys
try :
	dirr = sys.argv[1]
	to = sys.argv[2]
except :
	print("run with dir and format to convert to")
	exit()
print("Converting files in {}".format(dirr))
for filename in os.listdir(dirr):
	name, ext = filename.split(".")
	if(ext == "ppm"):
		print("Converting ", name)
		os.system("convert {}{}.ppm {}{}.{}".format(dirr, name, dirr, name, to))
		os.system("rm {}{}.ppm".format(dirr, name))
