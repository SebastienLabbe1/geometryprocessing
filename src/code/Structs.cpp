#include "Structs.h"
#include <cfloat>
#include <cmath>
#include <math.h>
#include <iostream>
#include <Util.h>

Ray::Ray(const Vector& origin, const Vector& direction) : origin(origin), direction(direction) {
    frac = Vector((abs(direction[0]) < 0.00001) ? DBL_MAX : 1/direction[0], 
    (abs(direction[1]) < 0.00001) ? DBL_MAX : 1/direction[1], 
    (abs(direction[2]) < 0.00001) ? DBL_MAX : 1/direction[2]);
}

double Polygon::area() {
    double T = 0;
    if(vertices.size()) {
        for(int i = 1; i < int(vertices.size()) - 1; i ++) {
            if(vertices[i][2] != 0) std::cout << "non zero z" << std::endl;
            T += cross(vertices[i] - vertices[0], vertices[i+1] - vertices[0])[2];
        }
    }
    return T / 2;
}

double Polygon::distanceIntegral(Vector Pi) {
    if(!vertices.size()) return 0;
    int n = vertices.size();
    double ans = 0;
    for(int i = 0; i < n; i ++) {
        //Helped by Mathias
        const Vector a = vertices[i] - Pi, b = vertices[(i+1) % n] - Pi;
        ans += cross(a,b)[2] * (dot(a,a) + dot(a,b) + dot(b,b));
    }
    return ans / 12;
    /*
    if(!vertices.size()) return 0;
    double I = 0;
    Vector c[3];
    c[0] = vertices[0];
    for(int i = 1; i < int(vertices.size()) - 1; i ++) {
        if(vertices[i][2] != 0) std::cout << "non zero z" << std::endl;
        double x = 0;
        c[1] = vertices[i];
        c[2] = vertices[i+1];
        for(int k = 0; k < 3; k ++) {
            for(int l = k; l < 3; l ++) {
                x += dot(c[k] - Pi, c[l] - Pi);
            }
        }
        double T = cross(c[0] - c[1], c[2] - c[1])[2]/2;
        I += T * x;
    }
    return I / 6;
    */
}

void Polygon::print() {
    std::cout << "Polygon" << std::endl;
    for(auto x : vertices) {
        std::cout << std::endl;
        printv(x);
    }
}