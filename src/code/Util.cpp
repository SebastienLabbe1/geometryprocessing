#include "Util.h"
#include "World.h"
#include "Image.h"
#include "Camera.h"
#include "Math.h"

#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <vector>

#define RAND (uniform(engine))

double reflectedProportion(double n1, double n2, double proj) {
    double diff = n1 - n2;
    double summ = n1 + n2;
    double k = diff * diff / (summ * summ);
    double K = 1 - proj;
    return k + (1 - k) * K * K * K * K * K; 
}

double absd(double r) {
    return (r > 0) ? r : -r;
}
 
Vector absv(const Vector& a) {
    return Vector(absd(a[0]), absd(a[1]), absd(a[2]));
}

int binomial(int n, double p) {
    int k = 0;
    for(int i = 0; i < n; i ++) 
        if(RAND < p) k ++;
    return k;
}

void printv(Vector a) {
    std::cout << a[0] << " " << a[1] << " " << a[2] << std::endl;
}


World* loadWorld(std::string filename, bool randomTextures) {
    /*
    std::vector<Object*> objects;
    World* world = new World();
    std::ifstream file(std::string("../worlds/") + filename);
    std::string buf;
    Camera* camera = nullptr;
    do {
        file >> buf;
        if(buf == "Camera") {
            Vector origin, scene;
            double height, width, minIntensity, focusDistance, blurStrength, startingIntensity;
            int maxBounces, raysPerPixel;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> scene[0] >> scene[1] >> scene[2];
            file >> height >> width;
            file >> minIntensity >> maxBounces >> raysPerPixel >> startingIntensity;
            file >> focusDistance >> blurStrength;
            camera = new Camera(origin, scene, height, width, minIntensity, 
            focusDistance, blurStrength, startingIntensity, maxBounces, raysPerPixel);
        } else if(buf == "Light") {
            Vector origin, color;
            double intensity;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> color[0] >> color[1] >> color[2];
            file >> intensity;
            if(randomTextures) color = Vector::random();
            //color = Vector::random();
            //origin = Vector::random() - Vector(0, 0.5, 0.5);
            //origin *= 32;
            lights.push_back(Light(origin, color, intensity));
        } else if(buf == "SLight") {
            Vector origin, color;
            double intensity, radius;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> radius;
            file >> color[0] >> color[1] >> color[2];
            file >> intensity;
            if(randomTextures) color = Vector::random();
            color = Vector::random();
            origin = Vector::random() - Vector(0, 0.5, 0.5);
            origin *= 32;
            radius = 1 + RAND * 3;
            SLight* sl = new SLight(origin, radius, color, intensity);
            slights.push_back(*sl);
            //objects.push_back(&slights.back());
            world->insert(sl);
        } else if(buf == "Sphere") {
            Vector origin, color;
            double radius, albedo, opacity, refractionIndex;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> radius;
            file >> color[0] >> color[1] >> color[2];
            file >> albedo >> opacity >> refractionIndex;
            if(randomTextures) color = Vector::random();
            MaterialProperties properties(color, albedo, opacity, refractionIndex);
            Sphere* sphere = new Sphere(origin, radius, properties);
            world->insert(sphere);
        } else if(buf == "Mesh") {
            file >> buf;
            std::string meshname = std::string("../meshes/") + buf + ".stl";

            file >> buf;
            std::string imagename = std::string("../textures/") + buf + ".ppm";
            Image* image = nullptr;
            if(buf != "None") image = new Image(imagename);
            //if(randomTextures) image = Image::randomSmooth(1000, 1000, 0.1);
            if(randomTextures) image = Image::randomSpread(100, 100, 0.8, 0.8);

            Vector origin, color;
            double albedo, opacity, refractionIndex, size;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> size;
            file >> color[0] >> color[1] >> color[2];
            file >> albedo >> opacity >> refractionIndex;

            MaterialProperties properties(color, albedo, opacity, refractionIndex);
            Mesh* mesh = Mesh::loadStl(meshname, properties, image);
            mesh->move(origin, size);
            world->insert(mesh);
        } else if(buf == "Cube") {
            file >> buf;
            std::string imagename = std::string("../textures/") + buf + ".ppm";
            Image* image = nullptr;
            if(buf != "None") image = new Image(imagename);
            //if(randomTextures) image = Image::randomSmooth(1000, 1000, 0.1);
            if(randomTextures) image = Image::randomSpread(100, 100, 0.8, 0.8);

            Vector origin, diagonal, color;
            double albedo, opacity, refractionIndex;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> diagonal[0] >> diagonal[1] >> diagonal[2];
            file >> color[0] >> color[1] >> color[2];
            file >> albedo >> opacity >> refractionIndex;

            MaterialProperties properties(color, albedo, opacity, refractionIndex);
            Cube* cube = new Cube(origin, diagonal, properties, image);
            world->insert(cube);
        }
    } while(!file.eof());

    world->addLights(lights);
    world->addSLights(slights);
    world->camera = camera;
    */
    return nullptr;
}

/*
Mesh* Mesh::loadStl(std::string filename,
const MaterialProperties& properties, Image* image = nullptr) {
	std::ifstream file(filename);

    std::vector<Vector> vertices, normals, uvs;
	std::vector<Triangle> triangles;

	std::string buf;
	while(buf != "facet") file >> buf;
	while(buf == "facet") {
	    Vector n, v[3], uv[3];
		file >> buf >> n[0] >> n[1] >> n[2];
		file >> buf >> buf;
        for(int i = 0; i < 3; i ++) file >> buf >> v[i][0] >> v[i][1] >> v[i][2];
        for(int i = 0; i < 3; i ++) file >> buf >> uv[i][0] >> uv[i][1] >> uv[i][2];
		file >> buf >> buf >> buf;
        int vs = vertices.size();
        int ns = normals.size();
        int uvss = uvs.size();
        Triangle triangle({vs, vs + 1, vs + 2}, {ns, ns, ns}, {uvss, uvss + 1, uvss + 2});
        for(int i = 0; i < 3; i ++) vertices.push_back(v[i]);
        for(int i = 0; i < 3; i ++) uvs.push_back(uv[i]);
        normals.push_back(n);
        triangles.push_back(triangle);
	}

	Mesh* mesh = new Mesh(vertices, normals, uvs, triangles, properties, image);
	return mesh;
}
*/