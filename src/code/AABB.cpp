#include "AABB.h"
#include "Util.h"

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <vector>

#define EPS 10e-6

AABB::AABB(std::vector<AABB*> aabbs){
    spilt(aabbs);
}

void AABB::spilt(std::vector<AABB*> aabbs) {
    if(aabbs.size() < 3) {
        if(aabbs.size() > 0) right = aabbs[0];
        if(aabbs.size() > 1) left = aabbs[1];
        return;
    } 
    vmin = aabbs[0]->vmax;
    vmax = aabbs[0]->vmin;
    for(int i = 0; i < int(aabbs.size()); i ++) {
        vmin.compWiseMin(aabbs[i]->vmax);
        vmax.compWiseMax(aabbs[i]->vmin);
    }
    int axe = (vmax - vmin).maxI();
    double v = ((vmax + vmin)[axe] / 2);
    std::vector<AABB*> aabbsr(aabbs.size(), nullptr), aabbsl(aabbs.size(), nullptr);
    int il = 0, ir = 0;
    for(auto aabb : aabbs) {
        double tv = (aabb->vmax[axe] + aabb->vmin[axe]) / 2;
        if(tv > v) {
            aabbsl[il] = aabb;
            il ++;
        } else {
            aabbsr[ir] = aabb;
            ir ++;
        }
    }
    while(!aabbsr.back()) aabbsr.pop_back();
    while(!aabbsl.back()) aabbsl.pop_back();
    if(aabbs.size() == aabbsl.size() || aabbsr.size() == aabbs.size()) {
        std::cout << aabbs.size() << std::endl;
        std::cout << aabbsr.size() << std::endl;
        std::cout << aabbsl.size() << std::endl;
        printv(vmax);
        printv(vmin);
        printv(vmax - vmin);
        std::cout << axe << " " << v << std::endl;
    }
    left = new AABB(aabbsl);
    right = new AABB(aabbsr);
}

bool AABB::intersect(const Ray& ray, Intersection& intersection) const {
    if(ray.direction.hasnan()) throw std::runtime_error("ray dir in AABB intersect has nan");

    float t1 = (vmin[0] - ray.origin[0])*ray.frac[0];
    float t2 = (vmax[0] - ray.origin[0])*ray.frac[0];
    float t3 = (vmin[1] - ray.origin[1])*ray.frac[1];
    float t4 = (vmax[1] - ray.origin[1])*ray.frac[1];
    float t5 = (vmin[2] - ray.origin[2])*ray.frac[2];
    float t6 = (vmax[2] - ray.origin[2])*ray.frac[2];

    float tmax = std::min(std::min(std::max(t1, t2), std::max(t3, t4)), std::max(t5, t6));
    if(tmax < EPS) return false;
    float tmin = std::max(std::max(std::min(t1, t2), std::min(t3, t4)), std::min(t5, t6));
    if(tmax - tmin < EPS || tmin > intersection.t) return false;
    return ((right && right->intersect(ray, intersection)) 
    | (left && left->intersect(ray, intersection)));
}

void AABB::setObject(Object* object) {
    //throw std::runtime_error("Still not sure if this is an error of i have no idea what else");
}

void AABB::computeAABB() {
    if(right) {
        right->computeAABB();
        if(left) {
            left->computeAABB();
            vmax = compWiseMax(left->vmax, right->vmax);
            vmin = compWiseMin(left->vmin, right->vmin);
        } else {
            vmax = right->vmax;
            vmin = right->vmin;
        }
    } else if(left) {
        left->computeAABB();
        vmax = left->vmax;
        vmin = left->vmin;
    } else {
        vmax.clear();
        vmin.clear();
    }
}