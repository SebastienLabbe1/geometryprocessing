#include "World.h"
#include "Util.h"
#include "Image.h"
#include "Camera.h"
#include "Structs.h"
#include "Object.h"
#include "Math.h"

#include <iostream>
#include <fstream>
#include <math.h>

#define RAND (uniform(engine))
#define EPS 10e-6

World::World(AABB* root, Camera* camera) : root(root), camera(camera) {

}

void World::fillImage(Image* const image) const {
    if(!camera) throw std::runtime_error("FillImage called with null camera");
    if(!image) throw std::runtime_error("FillImage called with null image ");
    root->computeAABB();
    const double di = 1. / image->height;
    const double dj = 1. / image->width;
    const double r = 1. - 1. / camera->raysPerPixel;
    const double ri = di * r;
    const double rj = dj * r;
    #pragma omp parallel for schedule(dynamic, 1)
    for(int i = 0; i < image->height; i ++)
    for(int j = 0; j < image->width; j ++) {
        const double li = i * di;
        const double lj = j * dj;
        Vector pixel;
        for (int ir = 0; ir < camera->raysPerPixel; ir ++) {
            Ray ray = camera->getRay(li + ri * RAND, lj + rj * RAND);
            if(ray.direction.hasnan()) throw std::runtime_error("ray dir has nan");
            pixel += getColor(ray, air.refractionIndex, 
            camera->maxBounces, camera->startingIntensity);
        }
        image->setPixel(i, j, pixel / (camera->raysPerPixel * camera->startingIntensity));
    }
}      

Vector World::getColorSimple(const Ray& ray, double refractionIndex, int bounces, int rayCount, 
bool isDiffused) const {
    //Get the next object in line of sight
    Intersection intersection;
    if (!root->intersect(ray, intersection)) {
        if(ray.direction[0] > 0) return Vector(0.529, 0.808, 0.922) * rayCount;
        return Vector();
    }
    if(!intersection.object) throw std::runtime_error("intersect returns true with no object");
    if(intersection.object->properties.isLight) return (isDiffused) ? Vector() 
    : (intersection.object->properties.color * rayCount 
    * std::max( - dot(ray.direction, intersection.normal), 0.));

    //Check if the ray collides from inside or outside
    double proj = dot(intersection.normal, ray.direction);
    double nextRefractionIndex = intersection.object->properties.refractionIndex;
    if (proj > 0) {
        nextRefractionIndex = air.refractionIndex;
        intersection.normal *= -1;
        proj *= -1;
    }

    Vector collision = ray.origin + ray.direction * intersection.t;
    Vector color;
    double albedo = intersection.object->properties.albedo;

    int diffractedRays = (bounces) ? binomial(rayCount, albedo) : rayCount;

    if(diffractedRays > 0 && intersection.color.norm2()) {
        int diffusedRays = (bounces) ? binomial(diffractedRays, directProportion) : diffractedRays;
        Vector totalLight;

        Ray lightRay = Ray(collision, sunDirection);

        Intersection lightIntersection;
        if(!root->intersect(lightRay, lightIntersection)) 
            totalLight += sunColor * dotPos(intersection.normal, sunDirection) * albedo;
        color = intersection.color.compWiseMult(totalLight) * diffusedRays;
    } 
    int bouncedRays = rayCount - diffractedRays;
    if(bouncedRays > 0) {
        double R = reflectedProportion(refractionIndex, nextRefractionIndex, abs(proj));
        int reflectedRays = binomial(bouncedRays, R);
        if(reflectedRays > 0) {
            Ray reflectedRay = Ray(collision, ray.direction.reflect(intersection.normal, proj));
            color += getColor(reflectedRay, refractionIndex, bounces - 1, reflectedRays);
        }
        int refractedRays = bouncedRays - reflectedRays;
        if(refractedRays > 0) {
            Ray refractedRay = Ray(collision, ray.direction.refract(intersection.normal,
            proj, refractionIndex, nextRefractionIndex));
            color += getColor(refractedRay, nextRefractionIndex, bounces - 1, refractedRays);
        }
    }
    return color;
}

Vector World::getColor(const Ray& ray, double refractionIndex, int bounces, int rayCount, 
bool isDiffused) const {
    if(ray.direction.hasnan()) throw std::runtime_error("ray dir in get color has nan");
    //Get the next object in line of sight
    Intersection intersection;
    if (!root->intersect(ray, intersection)) 
        return sunColor * std::max(dot(ray.direction, sunDirection), 0.);
    if(!intersection.object) throw std::runtime_error("intersect returns true with no object");
    if(intersection.object->properties.isLight) return (isDiffused) ? Vector() 
    : (intersection.object->properties.color * rayCount 
    * std::max( - dot(ray.direction, intersection.normal), 0.));

    //Check if the ray collides from inside or outside
    double proj = dot(intersection.normal, ray.direction);
    double nextRefractionIndex = intersection.object->properties.refractionIndex;
    if (proj > 0) {
        nextRefractionIndex = air.refractionIndex;
        intersection.normal *= -1;
        proj *= -1;
    }

    Vector collision = ray.origin + ray.direction * intersection.t;
    Vector color;
    double albedo = intersection.object->properties.albedo;

    int diffractedRays = (bounces) ? binomial(rayCount, albedo) : rayCount;

    if(diffractedRays > 0 && intersection.color.norm2()) {
        int diffusedRays = (bounces) ? binomial(diffractedRays, directProportion) : diffractedRays;
        Vector totalLight;

        Ray lightRay = Ray(collision, sunDirection);

        //Direct lighting
        Intersection lightIntersection;
        if(!root->intersect(lightRay, lightIntersection)) 
            totalLight += sunColor * dotPos(intersection.normal, sunDirection) * albedo * diffusedRays;

        //Indirect lighting
        int phongRays = diffractedRays - diffusedRays;
        double diffusedRatio = rho_d / (rho_d + rho_s);
        double frac = (alpha + 8) / (8 * M_PI);
        for(int i = 0; i < phongRays; i ++) {
            if(RAND < diffusedRatio) {
                Ray diffusedRay = Ray(collision, intersection.normal.randomCos());
                if(diffusedRay.direction.hasnan()) throw std::runtime_error("ray dir in get color diffused has nan");
                totalLight += getColor(diffusedRay, refractionIndex, bounces - 1, 1, true) 
                * albedo / diffusedRatio;
            } else {
                double z; 
                Vector H = ray.direction.reflect(intersection.normal, proj).randomPow(alpha, z);
                Ray phongRay = Ray(collision, ray.direction.reflect(H));
                if(phongRay.direction.hasnan()) throw std::runtime_error("ray dir in get color phongRay has nan");
                if(dot(phongRay.direction, intersection.normal) < 0) continue;
                double coef = rho_s * frac * pow(dotPos(intersection.normal, 
                (phongRay.direction - ray.direction).normalized()), alpha);
                double pdf_pow = pow(z, alpha) * (alpha + 1) / (M_PI * 8 * dotPos(ray.direction, H));
                totalLight += getColor(phongRay, refractionIndex, bounces - 1, 1) 
                * coef * dotPos(intersection.normal, phongRay.direction)
                / ((1 - diffusedRatio) * pdf_pow);
            }
        }
        color = intersection.color.compWiseMult(totalLight);
    } 
    int bouncedRays = rayCount - diffractedRays;
    if(bouncedRays > 0) {
        double R = reflectedProportion(refractionIndex, nextRefractionIndex, absd(proj));
        int reflectedRays = binomial(bouncedRays, R);
        if(reflectedRays > 0) {
            Ray reflectedRay = Ray(collision, ray.direction.reflect(intersection.normal, proj));
                if(reflectedRay.direction.hasnan()) throw std::runtime_error("ray dir in get color reflectedRay has nan");
            color += getColor(reflectedRay, refractionIndex, bounces - 1, reflectedRays);
        }
        int refractedRays = bouncedRays - reflectedRays;
        if(refractedRays > 0) {
            Ray refractedRay = Ray(collision, ray.direction.refract(intersection.normal,
            proj, refractionIndex, nextRefractionIndex));
                if(refractedRay.direction.hasnan()) throw std::runtime_error("ray dir in get color refractedRay has nan");
            color += getColor(refractedRay, nextRefractionIndex, bounces - 1, refractedRays);
        }
    }
    return color;
}

World* World::loadWorld(std::string filename) {
    std::cout << "Starting to load " << filename << std::endl;
    filename = std::string("../worlds/") + filename + ".wd";
	std::ifstream file(filename);

    std::string buf;

    file >> buf;
    if(buf != "Camera") throw std::runtime_error("Not valid world file : no camera");
    Vector origin, sceneCenter;
    double height, width, focalDistance, blurStrength, initialIntensity, minIntensity;
    int maxBounces, raysPrePixel;
    file >> buf >> origin[0] >> origin[1] >> origin[2];
    file >> buf >> sceneCenter[0] >> sceneCenter[1] >> sceneCenter[2];
    file >> buf >> height;
    file >> buf >> width;
    file >> buf >> focalDistance;
    file >> buf >> blurStrength;
    file >> buf >> initialIntensity;
    file >> buf >> minIntensity;
    file >> buf >> maxBounces;
    file >> buf >> raysPrePixel;
    Camera* camera = new Camera(origin, sceneCenter, height, width, minIntensity, 
    focalDistance, blurStrength, initialIntensity, maxBounces, raysPrePixel);

    file >> buf;
    if(buf != "Objects") throw std::runtime_error("Not valid world file : no objects");
    int n;
    file >> n;
    std::vector<AABB*> aabbs(n);
    Vector pos;
    for(int i = 0; i < n; i ++) {
        std::cout << "Object : " << i << std::endl;
        file >> buf >> pos[0] >> pos[1] >> pos[2];
        aabbs[i] = Object::loadObject(buf, pos);
    }
    for(auto aabb : aabbs) aabb->computeAABB();
    AABB* root = new AABB(aabbs);

    std::cout << "Finished loading " << filename << std::endl;
    return new World(root, camera);
}
