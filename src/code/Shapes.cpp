#include "Shapes.h"
#include "Object.h"
#include "Util.h"

#include <stdexcept>
#include <cmath>
#include <math.h>
#include <iostream>

#define EPS 10e-6

void Shape::setObject(Object* object) {
    this->object = object;
}

bool Shape::intersect(const Ray& ray, Intersection& intersection) const {
    throw std::runtime_error("shape intersect should not be called");
}

void Shape::computeAABB() {
    throw std::runtime_error("shape compute aabb should not be called");
}

Triangle::Triangle(int* iI) {
    for(int i = 0; i < 3; i ++) iV[i] = iI[i];
    for(int i = 0; i < 3; i ++) iN[i] = iI[3+i];
    for(int i = 0; i < 3; i ++) iT[i] = iI[6+i];
}

Sphere::Sphere(int iC, int iR) : iC(iC), iR(iR) {

}

Cube::Cube(int iC, int iD) : iC(iC), iD(iD) {

};

bool Triangle::intersect(const Ray& ray, Intersection& intersection) const {
    //https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/barycentric-coordinates

    Vector x = object->getVector(iV[0], true);
    Vector y = object->getVector(iV[1], true);
    Vector z = object->getVector(iV[2], true);

    Vector N = cross(y - x, z - x);
    double denom = dot(N, N);
 
    double proj = dot(N, ray.direction);
    if(abs(proj) < EPS) return false;

    double t = dot(N, x - ray.origin)/ proj;
    if(t < EPS || t > intersection.t) return false;
 
    Vector collision = ray.origin + ray.direction * t;
    double u, v;

    bool c1 = dot(N, cross(y - x, collision - x)) < 0;
    if (c1 != ((u = dot(N, cross(z - y, collision - y))) < 0)) return false;
    if (c1 != ((v = dot(N, cross(x - z, collision - z))) < 0)) return false;

    if(c1) denom *= -1;
    u /= denom; 
    v /= denom; 

    intersection.normal = barycenterValue(object->getVector(iN[0]), 
    object->getVector(iN[1]), object->getVector(iN[2]), u, v);
    intersection.color = (object->texture) ? object->texture->getPixel(
        barycenterValue(object->getVector(iT[0]), object->getVector(iT[1]), 
        object->getVector(iT[2]), u, v)) : object->properties.color;
    intersection.object = object;
    intersection.t = t;
    return true;
}

bool Sphere::intersect(const Ray& ray, Intersection& intersection) const {
    double radius = object->getDouble(iR);
    Vector center = object->getVector(iC, true);
    double radius2 = radius * radius;
    Vector oc = center - ray.origin;
    double t = dot(ray.direction, oc);
    Vector collision = ray.origin + ray.direction * t;
    double distance2 = (collision - center).norm2();
    if(distance2 > radius2) return false;
    double b = std::sqrt(radius2 - distance2);
    if(t > 0) { if(oc.norm2() > radius2 + EPS) b *= -1; }
    else if(oc.norm2() > radius2 - EPS) b *= -1;
    t += b;
    if(t < EPS || t > intersection.t) return false;
    collision += ray.direction * b;
    intersection.color = object->properties.color;
    intersection.normal = (collision - center) / radius;
    intersection.object = object;
    intersection.t = t;
    return true;
}


bool Cube::intersect(const Ray& ray, Intersection& intersection) const {
    Vector t1, t2;
    for(int i = 0; i < 3; i ++) {
        if(abs(ray.direction[i]) < EPS) {
            if(vmax[i] - ray.origin[i] < -EPS || ray.origin[i] - vmin[i] < -EPS) return false;
            t1[i] = -DBL_MAX;
            t2[i] = DBL_MAX;
        } else {
            t1[i] = (vmin[i] - ray.origin[i])*ray.frac[i];
            t2[i] = (vmax[i] - ray.origin[i])*ray.frac[i];
        }
    }
    double tmax = compWiseMax(t1, t2).min();
    if(tmax < EPS) return false;
    double tmin = compWiseMin(t1, t2).max();
    if(tmax - tmin < EPS) return false;
    double t = (tmin < EPS) ? tmax : tmin;
    if(t > intersection.t) return false;

    Vector collision = ray.origin + ray.direction * t;
    Vector normal;
    Vector uv;

    bool found = false;
    for(int i = 0; i < 3; i ++) {
        if(!found) {
            if(abs(collision[i] - vmax[i]) < EPS) {found = true; normal[i] = 1;}
            else if(abs(collision[i] - vmin[i]) < EPS) {found = true; normal[i] = -1;}
            else uv[i + 1] = (vmax[i] - collision[i]) / (vmax[i] - vmin[i]);
        } else uv[i] = (vmax[i] - collision[i]) / (vmax[i] - vmin[i]);
    }

    intersection.color = (object->texture) ? object->texture->getPixel(uv) : object->properties.color;
    intersection.normal = normal;
    intersection.object = object;
    intersection.t = t;
    return true;
}

void Triangle::computeAABB() {
    Vector x = object->getVector(iV[0], true);
    Vector y = object->getVector(iV[1], true);
    Vector z = object->getVector(iV[2], true);
    vmin = compWiseMin(compWiseMin(x, y), z);
    vmax = compWiseMax(compWiseMax(x, y), z);
}

void Sphere::computeAABB() {
    Vector center = object->getVector(iC, true);
    double radius = object->getDouble(iR);
    vmin = center - Vector(radius, radius, radius);
    vmax = center + Vector(radius, radius, radius);
}

void Cube::computeAABB() {
    Vector center = object->getVector(iC, true);
    Vector diagonal = object->getVector(iD);
    vmin = center - diagonal;
    vmax = center + diagonal;
}