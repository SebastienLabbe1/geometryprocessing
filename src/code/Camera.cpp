#include "Camera.h"
#include "Math.h"

#include <cmath>
#include <iostream>

#define RAND (uniform(engine))

Camera::Camera(Vector origin, Vector sceneCenter, double height, double width,
double minIntensity, double focusDistance, double blurStrength, double startingIntensity,
int maxBounces, int raysPerPixel) : height(height), width(width), minIntensity(minIntensity), 
focusDistanceInv(1./focusDistance), blurStrength(blurStrength), startingIntensity(startingIntensity), 
maxBounces(maxBounces), raysPerPixel(raysPerPixel) {
    this->origin = origin;
    this->sceneCenter = sceneCenter;
    direction = sceneCenter - origin;
    direction.normalize();
    right = Vector(0, direction[2], - direction[1]);
    right.normalize();
    up = cross(right, direction);
    up.normalize();
}

void Camera::rotate(double angle) {
    Vector temp = origin - sceneCenter;
    origin = rotateX(temp, angle) + sceneCenter;
    origin.compWiseMax(Vector(-16, -16, -16)).compWiseMin(Vector(16, 16, 16));
    direction = sceneCenter - origin;
    direction.normalize();
    right = Vector(0, direction[2], - direction[1]);
    right.normalize();
    up = cross(right, direction);
    up.normalize();
}

Ray Camera::getRay(double i, double j) const {
    double rightt = (j - 0.5) * width;
    double upt = (0.5 - i) * height;
    Vector blur = (up * (RAND - 0.5) + right * (RAND - 0.5)) * blurStrength;
    Vector dir = direction + up * upt + right * rightt - blur * focusDistanceInv;
    Vector ori = origin + blur;
    return Ray(ori, dir.normalized());
}