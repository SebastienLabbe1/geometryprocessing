#include "GeometryProcessing.h"
#include "Util.h"

#include <stdexcept>
#include <math.h>
#include <iostream>
#include <cassert>

#define EPS 10e-6

VoronoiInstance::VoronoiInstance(std::vector<Vector> p, std::vector<double> l, Polygon b) {
    points = p;
    lambda = l;
    boundingBox = b;
}

std::vector<Polygon> voronoi(std::vector<Vector> points, Polygon boundingBox) {
    //double M = 0;
    //for(auto point : points) M = std::max(point[2], M);
    //for(auto point : points) point[2] = sqrt(M - point[2]);
    //for(auto point : points) point[2] = M - point[2];
    int n = int(points.size());
    std::vector<double> wei(n);
    for(int i = 0; i < n; i ++) wei[i] = points[i][2];
    for(int i = 0; i < n; i ++) points[i][2] = 0;
    std::vector<Polygon> voronois(points.size());
    #pragma omp parallel for schedule(dynamic, 1)
    for(int iC = 0; iC < int(points.size()); iC++) {
        Vector C = points[iC];
        std::vector<Vector> cell = boundingBox.vertices;

        for(int iP = 0; iP < int(points.size()); iP++) {
            if(iP == iC) continue;

            Vector P = points[iP];
            Vector CP = P - C;

            //Vector N = CP.normalized();
            //double T = dot(C + (CP / 2), N);
            Vector N = CP.normalized();
            Vector M = C + CP * (.5 + (wei[iC]-wei[iP])*.5/dot(CP,CP));
            double T = dot(M, N);

            int n = cell.size();
            std::vector<double> ts(n);
            for(int i = 0; i < n; i ++) ts[i] = dot(N, cell[i]) - T;

            std::vector<Vector> ncell;
            for(int i = 0; i < n; i ++) {
                int j = (i+1) % n;
                int inter = 0;
                if(ts[i] < EPS) {
                    ncell.push_back(cell[i]);
                    if(ts[j] > -EPS) inter = 1;
                } else if(ts[j] < EPS) inter = 2;
                if(inter) {
                    Vector a = cell[i];
                    Vector b = cell[j];
                    Vector ab = b - a;
                    double abN = dot(ab, N);
                    Vector x;
                    if(abs(abN) < EPS) {
                        if(inter == 1) x = b;
                        else x = a;
                    } else {
                        double t = (T - dot(a, N)) / abN;
                        x = a + ab * t;
                    }
                    ncell.push_back(x);
                }
            }
            cell = ncell;
            for(int i = 0; i < n; i ++) cell[i][2] = 0;
        }
        voronois[iC].vertices = cell;
    }
    return voronois;
}

lbfgsfloatval_t evaluate(
    void *instance,
    const lbfgsfloatval_t *x,
    lbfgsfloatval_t *g,
    const int n,
    const lbfgsfloatval_t step
    )
{
    auto vInstance = (VoronoiInstance *) instance;
    auto points = vInstance->points;
    auto lambda = vInstance->lambda;
    auto boundingBox = vInstance->boundingBox;
    for(int i = 0; i < n; i++) points[i][2] = x[i];
    auto voronois = voronoi(points, boundingBox);
    for(int i = 0; i < n; i++) points[i][2] = 0;
    double sx = 0;
    lbfgsfloatval_t fx = 0.0;
    for(int i = 0; i < n; i++) {
        double T = abs(voronois[i].area());
        sx += T;
        g[i] = T - lambda[i];
        double aT = voronois[i].distanceIntegral(points[i]);
        fx += x[i] * g[i] - abs(aT);
    }
    return fx;
}

int progress(
    void *instance,
    const lbfgsfloatval_t *x,
    const lbfgsfloatval_t *g,
    const lbfgsfloatval_t fx,
    const lbfgsfloatval_t xnorm,
    const lbfgsfloatval_t gnorm,
    const lbfgsfloatval_t step,
    int n,
    int k,
    int ls
    )
{
    printf("Iteration %d:\n", k);
    printf("  fx = %f, x[0] = %f, x[1] = %f\n", fx, x[0], x[1]);
    printf("  xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
    printf("\n");
    return 0;
}

std::vector<double> semiDiscreteOptimalTransport(std::vector<Vector> points, 
    std::vector<double> lambda, Polygon boundingBox) {

    int N = points.size();
    //if (N != lambda.size()) 
        //throw std::runtime_error("Semi Discrete Optimal Transport needs as many lambdas as points");
    auto instance = new VoronoiInstance(points, lambda, boundingBox);

    lbfgsfloatval_t fx;
    lbfgsfloatval_t *x = lbfgs_malloc(N);
    lbfgs_parameter_t param;

    for (int i = 0; i < N; i++) x[i] = 0;

    lbfgs_parameter_init(&param);
    std::cout << "Calling lbfgs" << std::endl;
    int ret = lbfgs(N, x, &fx, evaluate, progress, instance, &param);
    std::cout << "lbfgs exit code " << ret << std::endl;

    std::vector<double> weights(N);
    for (int i = 0; i < N; i++) weights[i] = x[i];
    std::cout <<"Weights 0 : " << weights[0] << std::endl;

    lbfgs_free(x);

    return weights;
}

std::vector<Vector> tutte(std::vector<Vector> points, std::vector<std::vector<int>> adjcancy,
std::vector<int> boundary) {
    int n = points.size();
    int bn = boundary.size();
    double s = 0;
    std::vector<double> ss = std::vector<double>(bn);
    for(int i = 0; i < bn; i ++) {
        ss[i] = (points[boundary[i]] - points[boundary[(i + 1) % bn]]).norm();
        s += ss[i];
    }
    std::vector<Vector> p1,p2;
    p1 = points;
    p2 = points;
    double cs = 0;
    for(int i = 0; i < bn; i ++) {
        double teta = 2 * M_PI * cs / s;
        p1[boundary[i]] = Vector(cos(teta), sin(teta));
        cs += ss[i];
    }
    int niter = 100;
    for(int t = 0; t < niter; t ++) {
        for(int i = 0; i < n; i ++) {
            p2[i] = Vector();
            for(int neig : adjcancy[i]) p2[i] += p1[neig];
            p2[i] /= double(adjcancy[i].size());
        }
        for(int ib : boundary) p2[ib] = p1[ib];
        p1 = p2;
    }
    return p1;
}