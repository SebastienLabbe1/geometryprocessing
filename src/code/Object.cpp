#include "Object.h"

#include <iostream>
#include <fstream>
#include <stdexcept>

Object::Object(const MaterialProperties& properties, Image* texture, std::vector<AABB*> aabbs,
std::vector<Vector> vectors, std::vector<double> doubles, Vector pos) : AABB(), properties(properties),
texture(texture), vectors(vectors), doubles(doubles), translation(pos) {
    for(auto aabb : aabbs) aabb->setObject(this);
    for(auto aabb : aabbs) aabb->computeAABB();
    root = new AABB(aabbs);
}

Vector Object::getVector(int iV, bool transform) {
    if(transform) return vectors[iV] + translation;
    return vectors[iV]; //TODO add transformation
}

double Object::getDouble(int iD) {
    return doubles[iD]; //TODO add transformation
}

bool Object::intersect(const Ray& ray, Intersection& intersection) const {
    return root->intersect(ray, intersection);
}

void Object::computeAABB() {
    root->computeAABB();
    vmin = root->vmin;
    vmax = root->vmax;
}


Object* Object::loadObject(std::string filename, Vector pos) {
    std::cout << "Starting to load " << filename << std::endl;
    filename = std::string("../worlds/Objects/") + filename + ".obj";
	std::ifstream file(filename);

    std::string buf;

    file >> buf;
    if(buf != "Properties") throw std::runtime_error("Not valid obj file : no properties");

    MaterialProperties properties;

    file >> buf >> properties.color[0] >> properties.color[1] >> properties.color[2];
    file >> buf >> properties.albedo;
    file >> buf >> properties.opacity;
    file >> buf >> properties.refractionIndex;
    file >> buf >> buf;
    Image* image = nullptr;
    if(buf != "null") image = Image::loadPpm(buf);

    int n;

    file >> buf >> n;
    if(buf != "Vectors") throw std::runtime_error("Not valid obj file");

    std::vector<Vector> vectors;
    for(int i = 0; i < n; i ++) {
        Vector v;
        file >> v[0] >> v[1] >> v[2];
        vectors.push_back(v);
    }

    file >> buf >> n;
    if(buf != "Doubles") throw std::runtime_error("Not valid obj file");

    std::vector<double> doubles;
    for(int i = 0; i < n; i ++) {
        double x;
        file >> x;
        doubles.push_back(x);
    }

    file >> buf >> n;
    if(buf != "Shapes") throw std::runtime_error("Not valid obj file");

    std::vector<AABB*> aabbs;
    for(int i = 0; i < n; i ++) {
        file >> buf;
        if(buf == "Cube") {
            int iC, iD;
            file >> iC >> iD;
            aabbs.push_back(new Cube(iC, iD));
        } else if(buf == "Triangle") {
            int iI[9];
            file >> iI[0] >> iI[1] >> iI[2];
            file >> iI[3] >> iI[4] >> iI[5];
            file >> iI[6] >> iI[7] >> iI[8];
            aabbs.push_back(new Triangle(iI));
        } else if(buf == "Sphere") {
            int iC, iR;
            file >> iC >> iR;
            aabbs.push_back(new Sphere(iC, iR));
        } else throw std::runtime_error("Not valid obj file");
    }

    return new Object(properties, image, aabbs, vectors, doubles, pos);
}