#include "All.h"

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <iomanip>

double gaussian(Vector x, Vector c, double sigma) {
    return  exp(- (c - x).norm2()/(2 * sigma * sigma)) / (sqrt(2 * M_PI) * sigma);
}


int main(int argc, char **argv) {
    std::vector<Vector> vertices = {
        Vector(0, 0, 0),
        Vector(1, 0, 0),
        Vector(1, 1, 0),
        Vector(0, 1, 0)
    };
    Vector center = Vector(0.5, 0.5, 0);

    Polygon BB;
    BB.vertices = vertices;

    int n = 20;
    int N = n * n;

    std::vector<Vector> points(N);
    std::vector<double> lambdas(N);
    double sigma = 0.1;
    //double mmax = gaussian(center, center, sigma);
    for(int i = 0; i < N; i ++) {
        Vector x = Vector::random();
        //Vector x = Vector((double)((i/n) + 0.5) / n, (double)((i % n) + 0.5) / n, 0);
        x[2] = 0;
        points[i] = x;
        lambdas[i] = gaussian(x, center, sigma);
        //lambdas[i] = 1./N;
        points[i][2] = lambdas[i];
    }
    double M = 0;
    for(int i = 0; i < N; i ++) M += lambdas[i];
    for(int i = 0; i < N; i ++) lambdas[i] /= M;

    std::cout << "Start " << std::endl;
    auto begin = std::chrono::high_resolution_clock::now();

    //Code goes here
    auto weights = semiDiscreteOptimalTransport(points, lambdas, BB);
    for(int i = 0; i < int(points.size()); i ++) points[i][2] = weights[i];
    auto voronois = voronoi(points, BB);


    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "End " << std::endl;

    std::cout << "Time taken : " << std::chrono::duration_cast<
    std::chrono::nanoseconds>(end-begin).count() / double(10e8) << " s" << std::endl;

    std::stringstream ss;
    ss << time(0);
    std::string ts = ss.str();
    std::string filename = std::string("../results/result") + ts + ".svg";
    std::string tempfilename = "../result.svg";

    save_svg(voronois, filename);
    save_svg(voronois, tempfilename);

    return 0;
}
