#ifndef UTIL_H
#define UTIL_H

#include "Math.h"

#include <string>

class World;

double reflectedProportion(double n1, double n2, double proj);

double absd(double r);

Vector absv(const Vector& r);

int binomial(int n, double p);

void printv(Vector a);

World* loadWorld(std::string filename, bool randomizeColors = true);

#endif // UTIL_H