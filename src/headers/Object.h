#ifndef OBJECT_H
#define OBJECT_H

#include "Math.h"
#include "Structs.h"
#include "Image.h"
#include "Shapes.h"

#include <vector>

class Object : public AABB {
    public:
    MaterialProperties properties;
        Image* texture = nullptr;
        std::vector<Vector> vectors;
        std::vector<double> doubles;
        Matrix rotation;
        Vector translation;
        AABB* root = nullptr;

        Object(const MaterialProperties& properties, Image* texture, std::vector<AABB*> aabbs,
        std::vector<Vector> vectors, std::vector<double> doubles, Vector pos);

        Vector getVector(int iV, bool transform = false);
        double getDouble(int iD);
        
        static Object* loadObject(std::string filename, Vector pos);

        virtual bool intersect(const Ray& ray, Intersection& intersection) const;
        virtual void computeAABB();
};


#endif // OBJECT_H