#ifndef AABB_H
#define AABB_H

#include "Math.h"
#include "Structs.h"

#include <vector>

class CutPlane;

class AABB {
    public:
        Vector vmin, vmax;
        AABB* right = nullptr,* left = nullptr;

        AABB() = default;
        AABB(std::vector<AABB*> aabbs);

        void spilt(std::vector<AABB*> aabbs);

        //Virtual functions for shapes
        virtual void setObject(Object* object);
        virtual bool intersect(const Ray& ray, Intersection& intersection) const;
        virtual void computeAABB();
};

#endif // AABB_H