#ifndef SHAPES_H
#define SHAPES_H

#include "Math.h"
#include "AABB.h"
#include "Image.h"
#include "Structs.h"

class Shape : public AABB {
    public:
        Object* object = nullptr;

        Shape() = default;

        void setObject(Object* object);

        virtual bool intersect(const Ray& ray, Intersection& intersection) const;
        virtual void computeAABB();
};

class Triangle : public Shape {
    public:
	    int iV[3], iN[3], iT[3];

        Triangle(int* i);

        //AABB methods
        bool intersect(const Ray& ray, Intersection& intersection) const;
        void computeAABB();
};

class Sphere : public Shape {
    public:
        int iC, iR;

        Sphere(int iC, int iR);
        
        //AABB methods
        bool intersect(const Ray& ray, Intersection& intersection) const;
        void computeAABB();
};

class Cube : public Shape {
    public:
        int iC, iD;

        Cube(int iC, int iD);
        
        //AABB methods
        bool intersect(const Ray& ray, Intersection& intersection) const;
        void computeAABB();
};


#endif // SHAPES_H