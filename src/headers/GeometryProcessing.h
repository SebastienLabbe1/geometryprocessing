#include "Structs.h"
#include <stdio.h>
#include <lbfgs.h>

struct VoronoiInstance {
    std::vector<Vector> points;
    std::vector<double> lambda;
    Polygon boundingBox;
    VoronoiInstance(std::vector<Vector> p, std::vector<double> l, Polygon b);
};

std::vector<Polygon> voronoi(std::vector<Vector> points, Polygon boundingBox);

lbfgsfloatval_t evaluate(
    void *instance,
    const lbfgsfloatval_t *x,
    lbfgsfloatval_t *g,
    const int n,
    const lbfgsfloatval_t step
    );

int progress(
    void *instance,
    const lbfgsfloatval_t *x,
    const lbfgsfloatval_t *g,
    const lbfgsfloatval_t fx,
    const lbfgsfloatval_t xnorm,
    const lbfgsfloatval_t gnorm,
    const lbfgsfloatval_t step,
    int n,
    int k,
    int ls
    );

std::vector<double> semiDiscreteOptimalTransport(std::vector<Vector> points, 
    std::vector<double> lambda, Polygon boundingBox);

std::vector<Vector> tutte(std::vector<Vector> points, std::vector<std::vector<int>> adjcancy,
std::vector<int> boundary);