#ifndef WORLD_H
#define WORLD_H

#include "AABB.h"
#include "Structs.h"

#include <vector>
#include <string>

class Image;
class Camera;

class World {
    public:
        AABB* root;
        Camera* camera;
        MaterialProperties air;
        const double rho_d = 1, rho_s = 0.1, alpha = 100, directProportion = 0.5;
        Vector sunDirection = Vector(3, 1, 0).normalized(), sunColor = Vector(1, 1, 1);

        World(AABB* root = nullptr, Camera* camera = nullptr);

        void fillImage(Image* const image) const;
        Vector getColorSimple(const Ray& ray, double refractionIndex, int bounces, int rayCount, 
        bool isDiffused = false) const;
        Vector getColor(const Ray& ray, double refractionIndex, int bounces, int rayCount, 
        bool isDiffused = false) const;

        static World* loadWorld(std::string filename);
};

#endif // WORLD_H