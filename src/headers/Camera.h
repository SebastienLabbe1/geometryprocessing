#ifndef CAMERA_H
#define CAMERA_H

#include "Math.h"
#include "Structs.h"

class Camera {
    public:
        Vector origin, direction, up, right, sceneCenter;
        double height, width, minIntensity, focusDistanceInv, blurStrength, startingIntensity;
        int maxBounces, raysPerPixel;

        Camera(Vector origin, Vector sceneCenter, double height, double width,
        double minIntensity, double focusDistance, double blurStrength, double startingIntensity,
        int maxBounces, int raysPerPixel);

        Ray getRay(double i, double j) const;
        void rotate(double angle);
};

#endif // CAMERA_H