#ifndef STRUCTS_H
#define STRUCTS_H

#include "Math.h"

#include <cfloat>

class Object;

struct Ray {
    Vector origin, direction, frac;
    Ray(const Vector& origin, const Vector& direction);
};

struct Intersection {
    Vector normal, color;
    const Object* object = nullptr;
    double t = DBL_MAX;
};

struct MaterialProperties {
    Vector color;
    double albedo, opacity, refractionIndex = 1.0;
    bool isLight = false;
};

struct Polygon {  
	std::vector<Vector> vertices;

    double area();
    double distanceIntegral(Vector Pi);

    void print();
};	


#endif // STRUCTS_H